const express = require('express');
const path = require('path');
const http = require('http');
const morgan = require('morgan');
const favicon = require('serve-favicon');
const app = express();

app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));  // 'dev', 'short', 'tiny'. or no argument (default)
app.use(favicon(path.join(__dirname, '/favicon.ico')));

app.get('/hello', function(req, res) {
	console.log("hi m in hello");
	res.sendFile(path.join(__dirname, 'nodejs_hello.html'));
});
app.get('/checkurl', function(req, res) {
	console.log("hi m in nodejs checkurl");
	res.sendFile(path.join(__dirname, 'nodejs_checkurl.html'));
});
app.get('*', function (req, res) {
	console.log("hi m in index");
	res.sendFile(path.join(__dirname, 'index.html'));
});




const server = http.createServer(app).listen(8090, function(err) {
  if (err) {
    console.log(err);
  } else {
    const host = server.address().address;
    const port = server.address().port;
    console.log(`Server listening on ${host}:${port}`);
  }
});